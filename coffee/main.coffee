@SmoosterExtra or (@SmoosterExtra = {})
jQuery.extend @SmoosterExtra,
  name: "smo-plugin-categories-admin"
  version: "0.1.0"
  debug: false

  loadFontAwesome: ->
    if $("body").size() > 0 && !window.fontawesomeloaded
      window.fontawesomeloaded = true
      if document.createStyleSheet
        document.createStyleSheet "http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
      else
        $("head").append $("<link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' type='text/css' media='screen' />")


  createToolbars: (element) ->
    smooster_extra = @
    smooster_extra_config = @config

    cat_container = $('<div></div>', {'style': 'position: absolute; top: 5%; left: 5%; width: 90%; height: 90%; background-color: rgba(255,255,255,0.8);', 'class': 'smo-category-container smo-style-allowed'})
    cat_ul = $('<ul></ul>', {'style': 'list-style-type: none; margin: 5%; display: block;', 'class': 'smo-style-allowed'})
    cat_li = $('<li></li>', {'style': 'display: block; width: 100%; max-width: 100%; text-align: left; margin-left: 0px;', 'class': 'smo-category-container-li smo-style-allowed'})
    cat_checkbox_label = $('<label>', {'style': '', 'class': 'smo-style-allowed'})
    cat_checkbox = $('<input>', {'style': 'margin-right: 5px;', 'type': 'checkbox', 'name': 'smo-category', 'class': 'smo-style-allowed'})

    categories_ui = ""
    $(smooster_extra_config.smo_glide_categories_admin.categories).each (index, value) ->
      categories_ui += cat_li.html(cat_checkbox_label.attr('for', value).html(cat_checkbox.attr('value', value).attr('id', value)).append(value))[0].outerHTML
    cat_container.html(cat_ul.html(categories_ui))

    $(smooster_extra_config.smo_glide_categories_admin.list_items, element).not('.smo-category-container-li').each (index, value) ->
      btn_edit = $('<a href="#"><i class="fa fa-pencil"></i></a>')

      $(value).addClass('smo-style-allowed');

      btn_edit.click (e) ->
        e.preventDefault()
        list_element = $($(this).closest('.smo-extra-toolbars').data('list_element'))

        clone_cat_container = $(cat_container).clone()
        all_cat_string = ""
        all_cat_string = list_element.attr('data-smo-categories')

        clone_cat_container.find('input[type=checkbox]').each (index, value) ->
          if all_cat_string
            if all_cat_string.indexOf($(value).val()) > -1
              $(@).prop('checked', true);
          $(value).change ->
            if $(@).is(':checked')
              all_cat_string = ""
              all_cat_string = list_element.attr('data-smo-categories')
              if all_cat_string
                all_cat_string = "#{all_cat_string} #{$(value).val()}" if all_cat_string.indexOf($(value).val()) == -1
              else
                all_cat_string = "#{$(value).val()}"
              list_element.attr('data-smo-categories', all_cat_string)
            else
              all_cat_string = ""
              all_cat_string = list_element.attr('data-smo-categories')
              all_cat_string = all_cat_string.replace("#{$(value).val()} ", "");
              all_cat_string = all_cat_string.replace("#{$(value).val()}", "");
              list_element.attr('data-smo-categories', $.trim(all_cat_string))

        if list_element.find('.smo-category-container').length > 0
          $('.smo-category-container').remove()
        else
          list_element.css('position','relative');
          $(list_element).append(clone_cat_container);

        if typeof smooster_extra_config.smo_glide_categories_admin.after_add == 'function'
          smooster_extra_config.smo_glide_categories_admin.after_add()

        smooster_extra.recreateToolbars(element)


      btn_container = $('<li></li>', {'style': 'margin-left: 0px; padding: 8px;'})
      buttons = $('<ul></ul>', {'style': 'list-style-type: none; margin-bottom: 0px;'})

      buttons_list = [btn_edit]
      $(buttons_list).each (index, value) ->
        value.css
          'color': 'black'
        buttons.append(btn_container.clone().html(value))

      toolbar = $('<div></div>', {'style': 'position: absolute; z-index: 10000; border: 1px solid grey; border-radius: 4px; background-color: white;', 'class': 'smo-extra-toolbars'})
      toolbar.append(buttons)

      toolbar_left = '0px'
      toolbar_left = $(value).offset().left - toolbar.width() if $(value).offset().left - toolbar.width() > 0

      toolbar.css
        'top': $(value).offset().top,
        'left': toolbar_left
      toolbar.data('list_element', value)
      $('body').append(toolbar)
      toolbar.hide()

      $(value).hover (->
        $('.smo-extra-toolbars').stop(true, true)
        $('.smo-extra-toolbars').hide()
        toolbar.fadeIn()
        return
      ), ->
        $('.smo-extra-toolbars').stop(true, true)
        toolbar.delay(250).fadeOut()
        return

      toolbar.hover (->
        $('.smo-extra-toolbars').stop(true, true)
        return
      ), ->
        $('.smo-extra-toolbars').stop(true, true)
        toolbar.delay(250).fadeOut()
        return

  destroyToolbars: ->
    $('.smo-extra-toolbars').remove()

  recreateToolbars: (element) ->
    @destroyToolbars()
    @createToolbars(element)

  editable_categories: ->

    #mercury_region_id = Mercury.region.element.attr('id')
    #slideshow_sections_admin = $("##{mercury_region_id}").parent()
    SmoosterExtra.loadFontAwesome()
    SmoosterExtra.createToolbars(SmoosterExtra.config.smo_glide_categories_admin.element)

    Mercury.on "save:extra_region", (event) =>
      $('.smo-category-container').remove()
      SmoosterExtra.destroyToolbars()

$(window).bind 'mercury:ready', ->
  SmoosterExtra.editable_categories()
